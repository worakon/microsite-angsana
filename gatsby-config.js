module.exports = {
  pathPrefix: '/exhibition',
  siteMetadata: {
    title: 'Gatsby Default Starter',
  },
  plugins: ['gatsby-plugin-react-helmet', 'gatsby-plugin-styled-components'],
};
