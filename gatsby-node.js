const path = require('path');
/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

// Implement the Gatsby API “createPages”. This is called once the
// data layer is bootstrapped to let plugins create pages from data.
exports.createPages = ({ boundActionCreators, graphql }) => {
  const { createPage } = boundActionCreators;
  const projects = [''];
  // const projects = ['', 'owner', 'prospect', 'club', 'hotel'];
  const blogPostTemplate = path.resolve('src/templates/main.js');
  // Query for markdown nodes to use in creating pages.

  projects.forEach(project => {
    createPage({
      path: `/${project}`,
      component: blogPostTemplate,
      context: {
        project,
      },
    });
  });
};
