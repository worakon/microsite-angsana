import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import './index.css';

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="Microsite Angsana Exhibition"
      meta={[
        { name: 'description', content: 'Microsite Angsana Exhibition' },
        { name: 'keywords', content: 'Microsite Angsana Exhibition' },
      ]}
    >
      <script
        async
        src="https://www.googletagmanager.com/gtag/js?id=AW-783430394"
      />
      <script>
        {`window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'AW-783430394');`}
      </script>
    </Helmet>
    <div>{children()}</div>
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.func,
};

export default TemplateWrapper;
