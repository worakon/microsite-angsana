import React from 'react'
import Link from 'gatsby-link'

class MainPage extends React.Component {
    state = {
        items: [],
    }
  componentDidMount() {
      fetch('https://us-central1-forviz-2017.cloudfunctions.net/cic/spaces/AoDwNEOmKYQzggGea8jd/entries?model=unit')
      .then(res => res.json())
      .then(res => {
        this.setState({
            items: res.data,
        })
      })
  }
    render() {
        const { items } = this.state;
      return (
        <div>
            <h1>Hi people</h1>
            <p>Welcome to your new Gatsby site.</p>
            <Link to="/">Go to page</Link>
            {items.map(item => <h5>{item.id}</h5>)}
      </div>
      );
  }
}

export default MainPage
