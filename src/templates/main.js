import React from 'react';
import styled, { keyframes } from 'styled-components';
import _ from 'lodash';
import { Form, Input, Select, Button, Carousel, Icon, Modal } from 'antd';
import renderHTML from 'react-render-html';
import ReactGA from 'react-ga';

import 'antd/dist/antd.css';
import '../styles/font/stylesheet.css';
import '../styles/edit-antd.css';
import LogoImg from '../images/logo.png';
import slideImage1 from '../images/slider-angsana/slide-1.jpg';
import slideImage2 from '../images/slider-angsana/slide-2.jpg';
import slideImage3 from '../images/slider-angsana/slide-3.jpg';
import slideImage4 from '../images/slider-angsana/slide-4.jpg';
import slideImage5 from '../images/slider-angsana/slide-5.jpg';
import country from '../data/country.json';
import Img5PercenImgEn from '../images/image5percenEn.png';
import Img5PercenImgZh from '../images/image5percenZh.png';

require('es6-promise').polyfill();
require('isomorphic-fetch');

const FormItem = Form.Item;
const { TextArea } = Input;
const { Option, OptGroup } = Select;

const LeftBlock = styled.div`
  @media (max-width: 1024px) {
    width: calc(100% - 322px);
  }
  @media (max-width: 768px) {
    width: 100vw;
    flex-shrink: 0;
  }
  overflow: hidden;
  position: relative;
`;
const SliderBlock = styled.div`
  width: 100%;
  height: calc(100vh - 91px);
  overflow: hidden;
  position: relative;
  @media (max-width: 1024px) {
    height: calc(100vh - 120px);
  }
  @media (max-width: 768px) {
    width: 100vw !important;
    height: 100vw !important;
    flex-shrink: 0;
  }
`;

const Topblock = styled.div`
  width: 100%;
  min-height: 91px;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  flex: 0 0 auto;
`;

const TextBlock = styled.div`
  flex: 1 1 auto;
  text-align: center;
  padding: 8px 0;
`;

const TextSlider = styled.div`
  position: absolute;
  bottom: 19px;
  display: flex;
  flex-flow: column;
  transition: left 3s, opacity 3.8s;
  left: ${props => (props.active ? '40px' : '-60%')};
  opacity: ${props => (props.active ? '1' : '0')};
  width: calc(100% - 200px);
  @media (max-width: 1024px) {
    left: 32px;
    width: calc(100% - 90px);
  }
  @media (max-width: 768px) {
    left: 16px;
    padding-left: 8px;
    bottom: 28px;
  }
`;

const TextSliderTop = styled.div`
  font-size: 24px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`;

const TextSliderBottom = styled.div`
  font-size: 12px;
  font-weight: 500;
  font-style: italic;
  font-stretch: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  @media (max-width: 768px) {
    font-size: 10px;
  }
`;

const PageSlider = styled.div`
  text-align: center;
  height: calc(100vh - 91px);
  background: #364d79;
  background-size: auto 100%;
  background-size: cover;
  background-position: center;
  width: calc(100vw - 354px);
  @media (max-width: 1024px) {
    width: calc(100vw - 322px);
    height: calc(100vh - 120px);
  }
  @media (max-width: 768px) {
    width: 100vw !important;
    height: 100vw !important;
    flex-shrink: 0;
  }
`;

const BtnControlSlider = styled.div`
  position: absolute;
  display: flex;
  bottom: 17px;
  right: 26px;
  @media (max-width: 1024px) {
    display: none;
  }
`;

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-flow: row nowrap;
  @media (max-width: 768px) {
    flex-flow: column nowrap;
    align-items: center;
    height: auto;
  }
`;

const RegisterBlock = styled.div`
  width: 354px;
  overflow-y: scroll;
  height: 100vh;
  background-color: #002b49;
  padding-top: 14px;
  display: flex;
  align-items: center;
  flex: 0 0 auto;
  flex-flow: column nowrap;
  @media (max-width: 1024px) {
    width: 322px;
  }
  @media (max-width: 768px) {
    width: 100%;
    border-top: none;
    height: auto;
  }
`;

const LangBlock = styled.div`
  width: 100%;
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.71;
  letter-spacing: normal;
  color: #333333;
  padding: 0 24px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 25px;
  @media (max-width: 1024px) {
    margin-bottom: 8px;
  }
  @media (max-width: 768px) {
    display: none;
  }
  @media (min-height: 1200px) {
    margin-bottom: 40px;
  }
`;

const LangBlockMobile = styled(LangBlock)`
  @media (min-width: 769px) {
    display: none;
  }
  @media (max-width: 768px) {
    display: flex;
    flex-shrink: 0;
    justify-content: center;
    margin-bottom: 42px;
    margin-top: 20px;
  }
`;

const LangSpan = styled.span`
  cursor: pointer;
  color: ${props => (props.active ? '#fff' : '#fff')};
  opacity: ${props => (props.active ? 1 : 0.3)};
  @media (max-width: 768px) {
    color: ${props => (props.active ? '#002B49' : '#333333')};
    font-size: 18px;
  }
`;
const SplitLine = styled.div`
  width: 1px;
  height: 16px;
  opacity: 0.2;
  background-color: #000000;
  margin: 0 12px;
  @media (max-width: 768px) {
    font-size: 20px;
    margin: 0 24px;
  }
`;

const Header = styled.h1`
  font-size: 16px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.25;
  letter-spacing: 0.5px;
  text-align: center;
  color: #002b49;
  margin-bottom: 20px;
  &.color-fff {
    color: #fff;
  }

  @media (max-width: 1170px) {
    margin-bottom: 8px;
  }
  ${props => {
    return props.nomargin ? 'margin: 0' : '';
  }};
`;

const Logo = styled.img`
  width: 200px;
  height: auto;
  object-fit: contain;
  @media (max-width: 768px) {
    display: none;
  }
`;
const ShineEffect = keyframes`
  0% {
    background-position: -170px 0;
  }

  50%, 100% {
    background-position: 30px 0;
  }
`;

const Mask = styled.div`
  position: relative;
  display:block;
  width: 200px;
  height: 87px;
  text-align:center;
  background: ;
  -webkit-filter: drop-shadow(1px 1px 2px rgba(0,0,0,0.1));
  background-position: -170px 0;
  animation: ${ShineEffect} 4s linear infinite;
  margin-left: 16px;
  &::after{
    content:'';
    position: absolute;
    pointer-events: none;
    top:0; left:0; right:0; bottom: 0;
    background: radial-gradient(0 0,circle farthest-side, rgba(255,255,255,0) 90%,rgba(255,255,255,.8) 98%,rgba(255,255,255,0) 100%) no-repeat;
    background: radial-gradient(circle farthest-side at 0 0, rgba(255,255,255,0) 90%,rgba(255,255,255,.8) 98%,rgba(255,255,255,0) 100%) no-repeat;
    background-position: inherit; 
    -webkit-mask: url('${LogoImg}') center;
    mask-size: 100% 100%;
  }
  @media (max-width: 768px) {
    display: none;
  }
`;

const LogoMobile = styled(Logo)`
  width: 172px;
  height: 79px;
  @media (min-width: 769px) {
    display: none;
  }
  @media (max-width: 768px) {
    display: block;
  }
`;

const MaskMobile = styled(Mask)`
  width: 172px;
  height: 79px;
  @media (min-width: 769px) {
    display: none;
  }
  @media (max-width: 768px) {
    display: block;
    margin-bottom: 16px;
  }
`;

const RegisterConetentBlock = styled.div`
  width: 100%;
  padding: 0 24px;
  display: flex;
  flex-flow: column nowrap;
  .ant-form {
    padding: 16px;
    background: #fff;
    margin-bottom: 16px;
  }
  @media (max-width: 1024px) {
    padding: 0 24px;
  }
`;
const Block50Left = styled.div`
  width: 50%;
  padding-right: 3px;
`;
const Block50Right = styled.div`
  width: 50%;
  padding-left: 3px;
`;
const BlockDue = styled.div`
  width: 100%;
  display: flex;
`;

const AppointmentPreferen = styled.span`
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #505a5a;
  margin: 12px 0;
  display: inline-block;
  @media (max-width: 768px) {
    font-size: 16px;
  }
`;
const LinkBlock = styled.div`
  width: 100%;
  display: flex;
  margin: 6px 0;
  justify-content: center;
  margin-top: ${props => {
    return props.first ? '0' : '6';
  }};
  @media (max-width: 1024px) {
    margin-top: 6px;
  }
`;
const LinkTo = styled.a`
  font-size: 12px;
  text-align: center;
  color: #cccccc;
  display: inline-block;
  padding: 0 8px;
  line-height: 1;
  &:hover,
  &:active,
  &:focus {
    text-decoration: none;
    color: #555555;
  }
`;

const dataLang = {
  description: {
    eventName: {
      en: 'Oceanview Property Showcase',
      zh: '望海轩房产展示',
    },
    date: {
      en: 'Sat 3rd - Sun 4th November 2018 <br/>Hilton Singapore',
      zh: '2018年11月3日至4日（周六至周日）<br/>新加坡希尔顿酒店',
    },
    venue: {
      en:
        '581 Orchard Road, Singapore 238883 <br/>Thailand & Singapore Room, Level 5, 11AM - 6PM',
      zh: '581乌节路，238883新加坡<br/>泰国及新加坡厅，5楼，11:00 – 18:00',
    },
  },
  title: {
    en: 'REGISTER YOUR INTEREST',
    zh: '请登记您的信息',
  },
  dropDownTitle: {
    en: ['Title *', 'MR.', 'MS.', 'MRS.'],
    zh: ['称谓 *', '先生', '小姐', '女士'],
  },
  firstName: {
    en: 'First name *',
    zh: '名 *',
  },
  lastName: {
    en: 'Last name *',
    zh: '姓 *',
  },
  email: {
    en: 'E-mail *',
    zh: '电子邮箱 *',
  },
  contactNumber: {
    en: 'Contact number',
    zh: '电话',
  },
  exhibitionDates: {
    en: [
      'Date *',
      'Saturday',
      'Sunday',
      'To be confirmed',
      'Will not attend (wish to receive infomation)',
    ],
    zh: [
      '日期 *',
      '星期六',
      '星期日',
      '待定',
      '不参加本次活动（但希望以后收到相关信息）',
    ],
  },
  contactTime: {
    en: [
      'Time *',
      '11.00 - 12.00',
      '12.00 - 13.00 ',
      '13.00 - 14.00 ',
      '14.00 - 15.00 ',
      '15.00 - 16.00 ',
      '16.00 - 17.00 ',
      '17.00 - 18.00',
      'To be confirmed',
    ],
    zh: [
      '时间 *',
      '11.00 - 12.00',
      '12.00 - 13.00 ',
      '13.00 - 14.00 ',
      '14.00 - 15.00 ',
      '15.00 - 16.00 ',
      '16.00 - 17.00 ',
      '17.00 - 18.00',
      '待定',
    ],
  },
  otherQuestion: {
    en: [
      'How did you hear about our event ? * ',
      {
        title: 'Newspapers',
        dropdown: [
          'The Business Times',
          'The Straits Times',
          'The Sunday Times',
        ],
      },
      {
        title: 'Email',
        dropdown: [
          'Email from Banyan Tree Residences / Laguna Property',
          'Email from Banyan Tree Hotels & Resorts',
        ],
      },
      {
        title: 'Website',
        dropdown: [
          'The Strait Times Web Banner',
          'Banyan Tree Associated Sites',
        ],
      },
      {
        title: 'Social Media',
        dropdown: ['Facebook', 'Google Search & Website Banner'],
      },
      {
        title: 'Other Sources',
        dropdown: ['Other'],
      },
    ],
    zh: [
      '您从何处得知我们的展会信息 *',
      {
        title: '报纸',
        dropdown: ['商业时报', '海峡时报', '星期日泰晤士报'],
      },
      {
        title: '电子邮件',
        dropdown: [
          '发自悦榕轩或乐古浪房产的邮件',
          '发自悦榕酒店及度假村的邮件',
        ],
      },
      {
        title: '网站',
        dropdown: ['海峡时报网页横幅广告', '悦榕集团旗下网站'],
      },
      {
        title: '社交媒体',
        dropdown: ['Facebook的', '谷歌搜索及网页横幅'],
      },
      {
        title: '其他来源',
        dropdown: ['其他来源'],
      },
    ],
  },
  Message: {
    en: 'Message',
    zh: '留言',
  },
  Submit: {
    en: 'Submit',
    zh: '提交',
  },
  countryOfResidences: {
    en: 'Country of residence *',
    zh: '常居国 *',
  },
  successHead: {
    en: 'Your message has been successfully sent! ',
    zh: '您的留言已提交成功，非常感谢！',
  },
  successContent: {
    en: 'We will contact you within 48 hours of your message submission. ',
    zh: '我们将在您留言后的48小时内与您联系。',
  },
  appointmentPreferen: {
    en: 'Appointment preference',
    zh: '推介会预约',
  },
  textOnSlide: [
    {
      textTitle: {
        en: '2 bedroom apartments, all with private pools',
        zh: '所有双卧室公寓带私人泳池',
      },
      textDetail: {
        en: '',
        zh: '',
      },
    },
    {
      textTitle: {
        en: 'All apartments feature lagoon and sea views',
        zh: '所有公寓以观湖和面海为特色',
      },
      textDetail: {
        en: '',
        zh: '',
      },
    },
    {
      textTitle: {
        en: "Situated within Laguna Phuket, Asia's Finest Integrated Resort",
        zh: '位于亚洲最好的综合度假胜地---普吉岛乐古浪',
      },
      textDetail: {
        en: '',
        zh: '',
      },
    },
    {
      textTitle: {
        en: '5% Guaranteed returns for 5 years*',
        zh: '前5年5%年收益*',
      },
      textDetail: {
        en: '*Terms & conditions apply  ',
        zh: '*适用条款与条件',
      },
    },
    {
      textTitle: {
        en:
          'Complimentary membership to Angsana Vacation Club and Laguna Phuket Golf Club*',
        zh: '椿梿会和普吉岛乐古浪高尔夫俱乐部免费会员*',
      },
      textDetail: {
        en: '*Terms & conditions apply  ',
        zh: '*适用条款与条件',
      },
    },
  ],
  elForPolicy: {
    textDetail: {
      en:
        'We use cookies on this site to improve your user experience. By continuing to use this site, you are agreeing to our',
      zh:
        '我们在此网站使用了cookies来改善你的用户体验。继续访问网站，表示您同意我们的',
    },
    link: {
      en: 'Privacy Policy',
      zh: '隐私政策。',
    },
    btn: {
      en: 'I ACCEPT',
      zh: '同意',
    },
  },
  viewFullSite: {
    en: 'View Full Site',
    zh: '阅读完整内容',
  },
  termOfUse: {
    en: 'Term of use',
    zh: '使用条款',
  },
  privacyPolicy: {
    en: 'Privacy Policy',
    zh: '隐私政策',
  },
  img5Percen: {
    en: Img5PercenImgEn,
    zh: Img5PercenImgZh,
  },
  required: {
    en: 'This filed is required.',
    zh: '星期日泰晤士报.',
  },
};

const hasErrors = fieldsError => {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
};

class Main extends React.Component {
  state = {
    currentLang: 'en',
    disabledSubmit: false,
    active: false,
    showPolicy: true,
  }
  componentDidMount() {
    const { pathContext } = this.props;
    const project = _.get(pathContext, 'project', '');
    const googleAnalytic = 'UA-127566126-1';
    ReactGA.initialize(googleAnalytic);
    ReactGA.pageview(window.location.pathname + window.location.search);
    const getLang = this.getParameterByName('lang');
    const lang = getLang === 'zh' ? 'zh' : 'en';

    this.setState({
      currentLang: lang,
    });

    setTimeout(() => {
      this.setState({
        active: true,
      });
    }, 300);

    this.Topblock.addEventListener('onresize', () => {
      console.log('aaa');
    });
    window.onresize = () => {
      this.resizeSlider();
    };
    this.resizeSlider();
  }

  getParameterByName = (name, url) => {
    let cUrl = url;
    if (!url) {
      cUrl = window.location.href;
    }
    const parameterName = name.replace(/[\[\]]/g, '\\$&');
    let regex = new RegExp(`[?&]${parameterName}(=([^&#]*)|&|#|$)`),
      results = regex.exec(cUrl);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  resizeSlider = () => {
    this.SliderBlock.style.setProperty(
      'height',
      `calc(100vh - ${this.Topblock.clientHeight}px)`,
    );
    const pageSlider = document.getElementsByClassName('page-slider');
    _.forEach(pageSlider, el => {
      el.style.setProperty(
        'height',
        `calc(100vh - ${this.Topblock.clientHeight}px)`,
      );
    });
  }

  sendEmail = values => {
    const url =
      'http://www.angsanaresidences.com/exhibition/sendmail/sendEmailTo.php';
    return fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(values),
    })
      .then(res => {
        return res.json();
      })
      .then(res => {
        this.setState({
          disabledSubmit: false,
        });
        if (_.get(res, 'data') === 'success') {
          this.success();
          return true;
        }
        return false;
      });
  }

  gtagReportConversion = url => {
    const callback = function () {
      if (typeof url !== 'undefined') {
        window.location = url;
      }
    };
    gtag('event', 'conversion', {
      send_to: 'AW-783430394/h4VjCMnToYsBEPrlyPUC',
      event_callback: callback,
    });
    return false;
  }

  handleSubmit = e => {
    const { pathContext } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const project = _.get(pathContext, 'project', '');
        const subject = `OCEANVIEW PROPERTY SHOWCASE ${
          project !== '' ? ` - ${_.capitalize(project)}` : ''
        }`;
        const sendEmailValue = {
          title: _.get(values, 'title', ''),
          firstName: _.get(values, 'firstName', ''),
          lastName: _.get(values, 'lastName', ''),
          emailAddress: _.get(values, 'email', ''),
          telephoneNumber: _.get(values, 'telephone', ''),
          countryOfResidences: _.get(values, 'countryOfResidences', ''),
          date: _.get(values, 'exhibitionDates', ''),
          time: _.get(values, 'contactTime', ''),
          howDoesYouHearAboutOurEvent: _.get(values, 'otherQuestion', ''),
          message: _.get(values, 'message', ''),
          __subject: subject,
          appkey: 'Angsana-Bintan-Register',
        };
        // console.log(sendEmailValue);
        // return true;
        this.btnSubmit.disabled = true;
        this.setState({
          disabledSubmit: true,
        });
        this.sendEmail(sendEmailValue);
        this.gtagReportConversion();
      }
    });
  }
  changeLang = changeTo => {
    // const project = '';
    const urlLang = changeTo === 'zh' ? '?lang=zh' : '?lang=en';
    window.location = `${window.location.origin}${
      window.location.pathname
    }${urlLang}`;
    // window.history.pushState('', '', `${project}${urlLang}`);
    // this.setState({
    //   currentLang: changeTo,
    // });
  }
  prevSlide = () => {
    this.slider.prev();
  }
  nextSlide = () => {
    this.slider.next();
  }
  success() {
    const { currentLang } = this.state;
    const modalMessage = Modal.success({
      title: `${_.get(dataLang, `successHead.${currentLang}`)}`,
      content: `${_.get(dataLang, `successContent.${currentLang}`)}`,
      onOk() {
        window.location.reload();
      },
    });
    setTimeout(() => {
      window.location.reload();
      modalMessage.destroy();
    }, 6000);
  }
  handleHidePolicy = () => {
    this.setState({
      showPolicy: false,
    });
  }
  render() {
    const { getFieldDecorator, getFieldsError } = this.props.form;
    const { currentLang, active, showPolicy } = this.state;
    // Only show error after a field is touched.

    return (
      <Container>
        <LangBlockMobile>
          <LangSpan
            active={currentLang === 'en'}
            onClick={() => {
              this.changeLang('en');
            }}
          >
            English
          </LangSpan>
          <SplitLine />
          <LangSpan
            active={currentLang === 'zh'}
            onClick={() => {
              this.changeLang('zh');
            }}
          >
            中文
          </LangSpan>
        </LangBlockMobile>
        <MaskMobile>
          <LogoMobile src={LogoImg} />
        </MaskMobile>
        <LeftBlock>
          <Topblock
            innerRef={el => {
              this.Topblock = el;
            }}
          >
            <Mask>
              <Logo src={LogoImg} />
            </Mask>
            <TextBlock>
              <Header nomargin>
                {_.get(dataLang, `description.eventName.${currentLang}`, '')}
              </Header>
              <BlockHead>
                <div>
                  <p
                    style={{ margin: 0 }}
                    dangerouslySetInnerHTML={{
                      __html: _.get(
                        dataLang,
                        `description.date.${currentLang}`,
                        '',
                      ),
                    }}
                  />
                </div>
                <div>
                  <p
                    style={{ margin: 0 }}
                    dangerouslySetInnerHTML={{
                      __html: _.get(
                        dataLang,
                        `description.venue.${currentLang}`,
                        '',
                      ),
                    }}
                  />
                </div>
              </BlockHead>
            </TextBlock>
          </Topblock>
          <SliderBlock
            innerRef={el => {
              this.SliderBlock = el;
            }}
          >
            <Carousel
              effect="fade"
              speed={3000}
              autoplaySpeed={5000}
              autoplay
              ref={slider => {
                this.slider = slider;
              }}
            >
              <div>
                <PageSlider
                  className="page-slider"
                  style={{
                    backgroundImage: `url(${slideImage1})`,
                    backgroundPosition: 'top right',
                  }}
                />
                <TextSlider active={active}>
                  <TextSliderTop>
                    {_.get(
                      dataLang,
                      `textOnSlide.0.textTitle.${currentLang}`,
                      '',
                    )}
                  </TextSliderTop>
                  <TextSliderBottom>
                    {_.get(
                      dataLang,
                      `textOnSlide.0.textDetail.${currentLang}`,
                      '',
                    )}
                  </TextSliderBottom>
                </TextSlider>
                <Img5Percen
                  src={_.get(dataLang, `img5Percen.${currentLang}`, '')}
                />
              </div>
              <div>
                <PageSlider
                  className="page-slider"
                  style={{ backgroundImage: `url(${slideImage2})` }}
                />
                <TextSlider active={active}>
                  <TextSliderTop>
                    {_.get(
                      dataLang,
                      `textOnSlide.1.textTitle.${currentLang}`,
                      '',
                    )}
                  </TextSliderTop>
                  <TextSliderBottom>
                    {_.get(
                      dataLang,
                      `textOnSlide.1.textDetail.${currentLang}`,
                      '',
                    )}
                  </TextSliderBottom>
                </TextSlider>
                <Img5Percen
                  src={_.get(dataLang, `img5Percen.${currentLang}`, '')}
                />
              </div>
              <div>
                <PageSlider
                  className="page-slider"
                  style={{ backgroundImage: `url(${slideImage3})` }}
                />
                <TextSlider active={active}>
                  <TextSliderTop>
                    {_.get(
                      dataLang,
                      `textOnSlide.2.textTitle.${currentLang}`,
                      '',
                    )}
                  </TextSliderTop>
                  <TextSliderBottom>
                    {_.get(
                      dataLang,
                      `textOnSlide.2.textDetail.${currentLang}`,
                      '',
                    )}
                  </TextSliderBottom>
                </TextSlider>
                <Img5Percen
                  src={_.get(dataLang, `img5Percen.${currentLang}`, '')}
                />
              </div>
              <div>
                <PageSlider
                  className="page-slider"
                  style={{ backgroundImage: `url(${slideImage4})` }}
                />
                <TextSlider active={active}>
                  <TextSliderTop>
                    {_.get(
                      dataLang,
                      `textOnSlide.3.textTitle.${currentLang}`,
                      '',
                    )}
                  </TextSliderTop>
                  <TextSliderBottom>
                    {_.get(
                      dataLang,
                      `textOnSlide.3.textDetail.${currentLang}`,
                      '',
                    )}
                  </TextSliderBottom>
                </TextSlider>
                <Img5Percen
                  src={_.get(dataLang, `img5Percen.${currentLang}`, '')}
                />
              </div>
              <div>
                <PageSlider
                  className="page-slider"
                  style={{ backgroundImage: `url(${slideImage5})` }}
                />
                <TextSlider active={active}>
                  <TextSliderTop>
                    {_.get(
                      dataLang,
                      `textOnSlide.4.textTitle.${currentLang}`,
                      '',
                    )}
                  </TextSliderTop>
                  <TextSliderBottom>
                    {_.get(
                      dataLang,
                      `textOnSlide.4.textDetail.${currentLang}`,
                      '',
                    )}
                  </TextSliderBottom>
                </TextSlider>
                <Img5Percen
                  src={_.get(dataLang, `img5Percen.${currentLang}`, '')}
                />
              </div>
            </Carousel>
            <BtnControlSlider>
              <div onClick={this.prevSlide}>
                <Icon type="left-square" style={{ fontSize: 36 }} />
              </div>
              <div onClick={this.nextSlide}>
                <Icon type="right-square" style={{ fontSize: 36 }} />
              </div>
            </BtnControlSlider>
          </SliderBlock>
        </LeftBlock>
        <RegisterBlock>
          <LangBlock>
            <LangSpan
              active={currentLang === 'en'}
              onClick={() => {
                this.changeLang('en');
              }}
            >
              English
            </LangSpan>
            <SplitLine />
            <LangSpan
              active={currentLang === 'zh'}
              onClick={() => {
                this.changeLang('zh');
              }}
            >
              中文
            </LangSpan>
          </LangBlock>
          <Header className="color-fff">
            {_.get(dataLang, `title.${currentLang}`)}
          </Header>
          <RegisterConetentBlock>
            <Form
              layout="inline"
              onSubmit={this.handleSubmit}
              style={{ flexShrink: 0 }}
            >
              <FormItem className="input" style={{ width: '100%' }}>
                {getFieldDecorator('title', {
                  rules: [
                    {
                      required: true,
                      message: _.get(dataLang, `required.${currentLang}`, ''),
                      // message: 'Please input your Title!'
                    },
                  ],
                })(
                  <Select
                    placeholder={_.get(
                      dataLang,
                      `dropDownTitle.${currentLang}.0`,
                      '',
                    )}
                  >
                    {_.map(
                      _.get(dataLang, `dropDownTitle.${currentLang}`, []),
                      (value, key) => {
                        if (key === 0) {
                          return null;
                        }
                        return (
                          <Option key={key} value={value}>
                            {value}
                          </Option>
                        );
                      },
                    )}
                  </Select>,
                )}
              </FormItem>
              <FormItem className="input" style={{ width: '100%' }}>
                {getFieldDecorator('firstName', {
                  rules: [
                    {
                      required: true,
                      message: _.get(dataLang, `required.${currentLang}`, ''),
                      // message: 'Please input your First name!'
                    },
                  ],
                })(
                  <Input
                    placeholder={_.get(dataLang, `firstName.${currentLang}`)}
                  />,
                )}
              </FormItem>
              <FormItem className="input" style={{ width: '100%' }}>
                {getFieldDecorator('lastName', {
                  rules: [
                    {
                      required: true,
                      message: _.get(dataLang, `required.${currentLang}`, ''),
                      // message: 'Please input your Last name!'
                    },
                  ],
                })(
                  <Input
                    placeholder={_.get(dataLang, `lastName.${currentLang}`)}
                  />,
                )}
              </FormItem>
              <FormItem className="input" style={{ width: '100%' }}>
                {getFieldDecorator('email', {
                  rules: [
                    {
                      type: 'email',
                      // message: 'The input is not valid Email address!',
                    },
                    {
                      required: true,
                      message: _.get(dataLang, `required.${currentLang}`, ''),
                      // message: 'Please input your Email address!',
                    },
                  ],
                })(
                  <Input
                    placeholder={_.get(dataLang, `email.${currentLang}`)}
                  />,
                )}
              </FormItem>
              <FormItem className="input" style={{ width: '100%' }}>
                {getFieldDecorator('telephone', {})(
                  <Input
                    placeholder={_.get(
                      dataLang,
                      `contactNumber.${currentLang}`,
                    )}
                  />,
                )}
              </FormItem>
              <FormItem className="input" style={{ width: '100%' }}>
                {getFieldDecorator('countryOfResidences', {
                  rules: [
                    {
                      required: true,
                      message: _.get(dataLang, `required.${currentLang}`, ''),
                      // message: 'Please input your Country of residences!'
                    },
                  ],
                })(
                  <Select
                    showSearch
                    placeholder={_.get(
                      dataLang,
                      `countryOfResidences.${currentLang}`,
                    )}
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {_.map(
                      _.get(country, 'countries.country', []),
                      (value, key) => {
                        return (
                          <Option
                            key={key}
                            value={_.get(value, 'countryName', '')}
                          >
                            {_.get(value, 'countryName', '')}
                          </Option>
                        );
                      },
                    )}
                  </Select>,
                )}
              </FormItem>
              <AppointmentPreferen>
                {_.get(dataLang, `appointmentPreferen.${currentLang}`, '')}
              </AppointmentPreferen>
              <BlockDue className="input">
                <Block50Left>
                  <FormItem className="input" style={{ width: '100%' }}>
                    {getFieldDecorator('exhibitionDates', {
                      rules: [
                        {
                          required: true,
                          message: _.get(
                            dataLang,
                            `required.${currentLang}`,
                            '',
                          ),
                          // message: _.get(dataLang, `required.${currentLang}`, '')
                        },
                      ],
                    })(
                      <Select
                        dropdownMatchSelectWidth={false}
                        placeholder={_.get(
                          dataLang,
                          `exhibitionDates.${currentLang}.0`,
                          '',
                        )}
                      >
                        {_.map(
                          _.get(dataLang, `exhibitionDates.${currentLang}`, []),
                          (value, key) => {
                            if (key === 0) {
                              return null;
                            }
                            return (
                              <Option key={key} value={value}>
                                {renderHTML(value)}
                              </Option>
                            );
                          },
                        )}
                      </Select>,
                    )}
                  </FormItem>
                </Block50Left>
                <Block50Right>
                  <FormItem className="input" style={{ width: '100%' }}>
                    {getFieldDecorator('contactTime', {
                      rules: [
                        {
                          required: true,
                          message: _.get(
                            dataLang,
                            `required.${currentLang}`,
                            '',
                          ),
                        },
                      ],
                    })(
                      <Select
                        dropdownMatchSelectWidth={false}
                        placeholder={_.get(
                          dataLang,
                          `contactTime.${currentLang}.0`,
                          '',
                        )}
                      >
                        {_.map(
                          _.get(dataLang, `contactTime.${currentLang}`, []),
                          (value, key) => {
                            if (key === 0) {
                              return null;
                            }
                            return (
                              <Option key={key} value={value}>
                                {value}
                              </Option>
                            );
                          },
                        )}
                      </Select>,
                    )}
                  </FormItem>
                </Block50Right>
              </BlockDue>
              <FormItem
                className="input otherQuestion"
                style={{ width: '100%' }}
              >
                {getFieldDecorator('otherQuestion', {
                  rules: [
                    {
                      required: true,
                      message: _.get(dataLang, `required.${currentLang}`, ''),
                    },
                  ],
                })(
                  <Select
                    placeholder={_.get(
                      dataLang,
                      `otherQuestion.${currentLang}.0`,
                      '',
                    )}
                  >
                    {_.map(
                      _.get(dataLang, `otherQuestion.${currentLang}`, []),
                      (value, key) => {
                        if (key === 0) {
                          return null;
                        }
                        return (
                          <OptGroup key={key} label={value.title}>
                            {_.map(
                              _.get(value, 'dropdown', []),
                              (valueDropdown, keyDropdown) => {
                                return (
                                  <Option
                                    key={`${key}-${keyDropdown}`}
                                    value={valueDropdown}
                                  >
                                    {valueDropdown}
                                  </Option>
                                );
                              },
                            )}
                          </OptGroup>
                        );
                      },
                    )}
                  </Select>,
                )}
              </FormItem>
              <FormItem className="input" style={{ width: '100%' }}>
                {getFieldDecorator('message', {})(
                  <TextArea
                    rows={4}
                    placeholder={_.get(dataLang, `Message.${currentLang}`, '')}
                  />,
                )}
              </FormItem>
              <FormItem className="input" style={{ width: '100%' }}>
                <Button
                  type="primary-custom"
                  htmlType="submit"
                  disabled={
                    this.state.disabledSubmit || hasErrors(getFieldsError())
                  }
                  ref={ref => {
                    this.btnSubmit = ref;
                  }}
                >
                  {_.get(dataLang, `Submit.${currentLang}`, '')}
                </Button>
              </FormItem>
              <LinkBlock first>
                <LinkTo
                  target="_blank"
                  href={`http://www.angsanaresidences.com/?lang=${currentLang}#termsofuse`}
                  style={{ borderRight: '1px solid #cccccc' }}
                >
                  {_.get(dataLang, `termOfUse.${currentLang}`, '')}
                </LinkTo>
                <LinkTo
                  target="_blank"
                  href={`http://www.angsanaresidences.com/?lang=${currentLang}#privacy-policy`}
                >
                  {_.get(dataLang, `privacyPolicy.${currentLang}`, '')}
                </LinkTo>
              </LinkBlock>
              <LinkBlock
                style={{ width: 'calc(100% + 50px)', marginLeft: '-25px' }}
              >
                <LinkTo
                  target="_blank"
                  href={`http://www.angsanaresidences.com?lang=${currentLang}`}
                >
                  {_.get(dataLang, `viewFullSite.${currentLang}`, '')}
                </LinkTo>
              </LinkBlock>
            </Form>
          </RegisterConetentBlock>
        </RegisterBlock>
        <Policy showPolicy={showPolicy}>
          <div>
            {_.get(dataLang, `elForPolicy.textDetail.${currentLang}`, '')}
            <LinkToPolicy
              target="_blank"
              href={`http://www.angsanaresidences.com/?lang=${currentLang}#privacy-policy`}
            >
              {_.get(dataLang, `elForPolicy.link.${currentLang}`, '')}
            </LinkToPolicy>
          </div>
          <Button
            type="primary-custom"
            style={{ width: 'auto', margin: 0 }}
            onClick={this.handleHidePolicy}
          >
            {_.get(dataLang, `elForPolicy.btn.${currentLang}`, '')}
          </Button>
        </Policy>
      </Container>
    );
  }
}
const WrappedHorizontalLoginForm = Form.create()(Main);
export default WrappedHorizontalLoginForm;

const Policy = styled.div`
  background: rgba(0, 0, 0, 0.8);
  padding: 8px 16px;
  position: fixed;
  width: 100%;
  bottom: 0;
  color: #fff;
  display: ${props => {
    return props.showPolicy ? 'flex' : 'none';
  }};
  align-items: center;
  justify-content: space-between;
`;

const LinkToPolicy = styled.a`
  font-size: 12px;
  text-align: center;
  color: #fff;
  padding: 8px 0;
  line-height: 1;
  display: inline-block;
  border-bottom: 1px solid #fff;
  margin-left: 8px;
  transition: all 0.3s;
  &:hover,
  &:active,
  &:focus {
    border-bottom: 1px solid #555555;
    text-decoration: none;
    color: #555555;
  }
  @media (max-width: 375px) {
    margin-left: 0;
  }
`;

const Img5Percen = styled.img`
  position: absolute;
  z-index: 10;
  top: 20px;
  right: 20px;
  width: 20%;
`;

const BlockHead = styled.div`
  display: flex;
  justify-content: center;
  flex-flow: row wrap;
  padding: 8px 16px;
  div {
    flex: 1 1 50%;
    display: flex;
    p {
      display: block;
      text-align: left;
      padding: 0 16px;
    }
    &:first-child {
      justify-content: flex-end;
      p {
        border-right: 2px solid #002b49;
        font-weight: bold;
      }
    }
  }

  @media (max-width: 1170px) {
    flex-flow: column;
    padding: 0;
    div {
      flex: 1 0 100%;
      display: flex;
      justify-content: center;
      p {
        text-align: center;
        padding: 4px 8px;
      }
      &:first-child {
        justify-content: center;
        p {
          border-right: none;
          font-weight: bold;
        }
      }
    }
  }
`;
